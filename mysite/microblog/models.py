from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django import forms

class Post(models.Model):
    title = models.CharField(max_length=50)
    text = models.CharField(max_length=500)
    pub_date = models.DateTimeField()
    mod_date = models.DateTimeField(blank=True, null=True)
    pub_user = models.ForeignKey(User, related_name='pub_users')
    mod_user = models.ForeignKey(User, related_name='mod_users', blank=True, null=True)

    def __unicode__(self):
        return self.title

class UserActivation(models.Model):
    user = models.ForeignKey(User)
    activation_key = models.CharField(max_length=6, null=True, blank=True)

    def __unicode__(self):
        return self.activation_key

class Tag(models.Model):
    name = models.CharField(max_length=25)
    articles = models.ManyToManyField(Post)

    def __unicode__(self):
        return self.name

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, widget=forms.TextInput())
    password = forms.CharField(max_length=50, widget=forms.PasswordInput(render_value=False))

class PostForm(forms.Form):
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(), required=False)
    title = forms.CharField(max_length=40, min_length=3)
    text = forms.CharField(max_length=300, min_length=10, widget=forms.Textarea())

class RegistrationForm(forms.Form):
    username = forms.CharField(max_length=40, widget=forms.TextInput())
    email = forms.EmailField(max_length=40, widget=forms.TextInput())
    password1 = forms.CharField(max_length=40, widget=forms.PasswordInput(render_value=False))
    password2 = forms.CharField(max_length=40, widget=forms.PasswordInput(render_value=False))

    def clean_username(self):
        try:
            user = User.objects.get(username=self.cleaned_data['username'])
        except User.DoesNotExist:
          return self.cleaned_data['username']
        raise forms.ValidationError('Jest juz w systemie taki uzytkownik')

    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError('Podaj dwa razy to samo haslo')
        return self.cleaned_data

