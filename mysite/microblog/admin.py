from django.contrib import admin
from microblog.models import Tag, Post, UserActivation
admin.site.register(Tag)
class PostAdmin(admin.ModelAdmin):
    search_fields = ('tag__name',)
    ordering = ('pub_date', )
    list_filter = ('pub_date', )

admin.site.register(UserActivation)
admin.site.register(Post, PostAdmin)